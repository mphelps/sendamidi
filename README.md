# sendAmidi
A way to make the Stream Deck send Midi commands (on Mac)

## Installation
Clone this project to your local drive. Go to the folder in Terminal and run the command below:
`chmod +x ./*.sh`

A virtual MIDI port is required. This functionality is built into macOS, instructions for your specific version can be found by searching for "mac virtual midi port" using your favorite search engine.

In the "sendAMidi.config" file, set the "portName" variable to the name of your virtual MIDI loopback port.

Stream Deck doesn't support flags on Mac, so we have one file per button. Files are named "sendAMidi_[row]_[column].sh". Add a "Run" action (under "System") to the button using the Stream Deck software and assign the corresponding file. The top left button would be "sendAMidi_0_0.sh", one button to the right would be "sendAMidi_0_1.sh", and so on.

## Config
The sendAMidi.config file provides the following options:
portName: The MIDI port we should send commands to.
enableReset: Enable sending the reset command. Disable if you only want the actual command and not the reset command followed by the actual command. Values: "true" or "false"
noteReset: This is the command used as the reset command.
