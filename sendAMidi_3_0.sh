#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. $DIR/sendAMidi.config

portNameLoc=$(echo "$portName" | tr -d '\r')
enableResetLoc=$(echo "$enableReset" | tr -d '\r')

cd $DIR

if [ "$enableResetLoc" == "true" ]
then
  echo "Send reset"
  . $DIR/sendAMidi_reset.sh
else
  echo "no reset"
fi

echo "sendmidi dev $portNameLoc on 30 1"
./sendmidi dev $portName on 30 1
