#!/bin/bash

# Get the script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Load the config file
. $DIR/sendAMidi.config

# Remove any newlines
portNameLoc=$(echo "$portName" | tr -d '\r')
noteResetLoc=$(echo "$noteReset" | tr -d '\r')

# Go to the script directory
cd $DIR

# Send the note
echo "sendmidi dev ${portNameLoc} ${noteResetLoc}"
./sendmidi dev ${portNameLoc} ${noteResetLoc}
